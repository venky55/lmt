from django.db import models
from django.shortcuts import render
# Create your models here.

class Employee(models.Model):
    name = models.TextField()
    voice_clarity = models.IntegerField(default = 0)
    vocabulary = models.IntegerField(default = 0)
    fluency = models.IntegerField(default = 0)

    

