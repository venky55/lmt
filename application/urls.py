from django.urls import path, include
from . import views

urlpatterns = [
    path('index',views.index),
    path('create',views.create),
    path('',views.home)
]
