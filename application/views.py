from django.shortcuts import render
from .models import Employee
# Create your views here.

def index(request):
  employees = Employee.objects.all()
  return render(request, 'index.html', {'employees' : employees})

def create(request):
  name = request.POST['name']
  # voice_clarity = int(request.POST['voice_clarity'])
  # vocabulary = int(request.POST['vocabulary'])
  # fluency = int(request.POST['fluency'])
  # employee = Employee.objects.create(name=name, voice_clarity=voice_clarity,vocabulary=vocabulary,fluency=fluency )
  employee = Employee.objects.create(name=name)
  return render(request, 'show.html', {'emp' : employee})

def home(request):
  # demo
  return render(request, 'home.html')